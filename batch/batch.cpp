#include "batch.hpp"
#include <boost/asio/connect.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/write.hpp>
#include <boost/regex.hpp>
#include <iostream>

using namespace std::literals;
namespace asio = boost::asio;
namespace ip = asio::ip;
namespace sys = boost::system;

namespace mcf::example {

static auto SEPARATOR = "\n----------------------\n"sv;
static auto CORRECT_CMD = "startup\n"sv;
static auto WRONG_CMD = "wrong\n"sv;

// g_和rand_用于生成0~3之间的伪随机数
Batch::Batch(asio::io_context& io, size_t c)
  : c_{c}, g_{std::random_device{}()}, rand_{0, 4}, r_{io}, s_{io}, buf_{}
{
}

void Batch::start(std::string_view host, std::string_view service)
{
  r_.async_resolve(host, service, [this](auto&& ec, auto&& rs) {
    if (ec)
      on_error(ec);
    else
      do_connect(rs);
  });
}

void Batch::on_error(sys::error_code const& ec)
{
  if (ec != asio::error::eof) {
    std::cout << "ERROR: " << ec.message() << "\n";
  }
  if (s_.is_open()) {
    s_.close();
  }
}

void Batch::do_connect(ip::tcp::resolver::results_type const& rs)
{
  asio::async_connect(s_, rs, [this](auto&& ec, auto) {
    if (ec)
      on_error(ec);
    else
      do_read_first();
  });
}

void Batch::do_read_first()
{
  // 处理MCF首次发回的数据，即'\n>'。
  asio::async_read_until(s_, buf_, "\n>"sv, [this](auto&& ec, auto len) {
    if (ec)
      on_error(ec);
    else if (len != 2 || buf_.size() != 2) {
      std::cout << "ERROR: \"\\n>\" is expected, but \"";
      std::copy_n(asio::buffers_begin(buf_.data()), len, std::ostream_iterator<char>{std::cout});
      std::cout << "\" received.";
    }
    else {
      buf_.consume(len);
      do_write();
    }
  });
}

void Batch::do_read()
{
  // '\r\n>'和'\r\n?'MCF单次输出数据的终止符，'\r\n>'表示命令执行完成，'\r\n?'表示命令执行出错。
  asio::async_read_until(s_, buf_, boost::regex{"\r\n[>?]$"}, [this](auto&& ec, auto len) {
    if (ec) {
      on_error(ec);
      return;
    }
    auto data = static_cast<char const*>(buf_.data().data());
    std::cout << "RECV : " << std::string_view{data, len - 3} << "\n";
    std::cout << "STATE: " << (data[len - 1] == '>' ? "SUCCESS" : "FAILED") << "\n";
    buf_.consume(len);
    do_write();
  });
}

void Batch::do_write()
{
  if (c_-- == 0) return;

  // 25%概率发送WRONG_CMD，75%的概率发送CORRECT_CMD。
  auto data = rand_(g_) == 0 ? WRONG_CMD : CORRECT_CMD;
  asio::async_write(s_, asio::buffer(data), [this, data](auto&& ec, auto) {
    if (ec) {
      on_error(ec);
    }
    else {
      std::cout << SEPARATOR;
      std::cout << "SEQ  : " << c_ << "\n";
      std::cout << "SENT : " << data;
      do_read();
    }
  });
}

}  // namespace mcf::example
