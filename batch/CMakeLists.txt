set(TGT batch)
add_executable(${TGT} main.cpp batch.cpp)
target_link_libraries(${TGT} PRIVATE Boost::system)
