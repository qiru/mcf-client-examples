#ifndef _MCF_EXAMPLE_BATCH_HPP_
#define _MCF_EXAMPLE_BATCH_HPP_

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/streambuf.hpp>
#include <random>
#include <stdint.h>
#include <string_view>

namespace mcf::example {

class Batch {
public:
  Batch(boost::asio::io_context&, size_t);

  void start(std::string_view, std::string_view);

private:
  void on_error(boost::system::error_code const&);
  void do_connect(boost::asio::ip::tcp::resolver::results_type const&);
  void do_read_first();
  void do_read();
  void do_write();

private:
  size_t c_;
  std::mt19937 g_;
  std::uniform_int_distribution<int> rand_;
  boost::asio::ip::tcp::resolver r_;
  boost::asio::ip::tcp::socket s_;
  boost::asio::streambuf buf_;
};

}  // namespace mcf::example

#endif  // _MCF_EXAMPLE_BATCH_HPP_
