#include "batch.hpp"
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc != 3) {
    std::cout << "Usage: batch <host> <port>\n";
    std::exit(1);
  }

  auto io = boost::asio::io_context{};
  auto batch = mcf::example::Batch{io, 5};
  batch.start(argv[1], argv[2]);

  io.run();
  return 0;
}
