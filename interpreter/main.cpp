#include "interpreter.hpp"
#include <iostream>

int main(int argc, char* argv[])
{
  if (argc != 3) {
    std::cout << "Usage: interpreter <host> <port>\n";
    std::exit(1);
  }

  auto io = boost::asio::io_context{};
  auto interpreter = mcf::example::Interpreter{io};
  interpreter.start(argv[1], argv[2]);

  io.run();
  return 0;
}
