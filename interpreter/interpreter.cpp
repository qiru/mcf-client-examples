#include "interpreter.hpp"
#include <boost/asio/connect.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/write.hpp>
#include <boost/regex.hpp>
#include <iostream>

namespace asio = boost::asio;
namespace ip = asio::ip;
namespace sys = boost::system;

namespace mcf::example {

Interpreter::Interpreter(asio::io_context& io) : r_{io}, s_{io}, buf_{} {}

void Interpreter::on_error(sys::error_code const& ec)
{
  if (ec != asio::error::eof) {
    std::cout << "ERROR: " << ec.message() << "\n";
  }
  if (s_.is_open()) {
    s_.close();
  }
}

void Interpreter::start(std::string_view host, std::string_view service)
{
  r_.async_resolve(host, service, [this](auto&& ec, auto&& rs) {
    if (ec)
      on_error(ec);
    else
      do_connect(rs);
  });
}

void Interpreter::do_connect(ip::tcp::resolver::results_type const& rs)
{
  asio::async_connect(s_, rs, [this](auto&& ec, auto) {
    if (ec)
      on_error(ec);
    else
      do_read();
  });
}

void Interpreter::do_read()
{
  /* MCF发回的数据有三种分隔符形式:
   *   - "\n>": 客户端连接后，MCF第一次发回的数据，不包含其它内容；
   *   - "\r\n>": 用户命令正常执行后，MCF发回命令的输出结果，并在结果后附上该分隔符；
   *   - "\r\n?": 用户命令执行出错，MCF发回错误信息(如果有)，并在错误信息后附上该分隔符。
   * 这里用boost::regex来描述三种分割符。
   */
  asio::async_read_until(s_, buf_, boost::regex{"^\\n>$|\\r\\n[>?]$"}, [this](auto&& ec, auto len) {
    if (ec)
      on_error(ec);
    else
      do_write(len);
  });
}

void Interpreter::do_write(size_t len)
{
  // 原样输出除提示符以外的内容
  std::copy_n(asio::buffers_begin(buf_.data()), len - 1, std::ostream_iterator<char>{std::cout});
  buf_.consume(len - 1);

  auto input = std::string{};
  do {
    // 输出提示符
    std::copy_n(asio::buffers_begin(buf_.data()), 1, std::ostream_iterator<char>{std::cout});
    std::getline(std::cin, input);
  } while (input.empty());
  buf_.consume(1);

  // 以'\n'结束一条输入命令
  input += "\n";
  asio::async_write(s_, asio::buffer(input), [this](auto&& ec, auto) {
    if (ec)
      on_error(ec);
    else
      do_read();
  });
}

}  // namespace mcf::example
