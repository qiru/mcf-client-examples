#ifndef _MCF_EXAMPLE_INTERPRETER_HPP_
#define _MCF_EXAMPLE_INTERPRETER_HPP_

#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/streambuf.hpp>
#include <stdint.h>
#include <string_view>

namespace mcf::example {

class Interpreter {
public:
  Interpreter(boost::asio::io_context&);

  void start(std::string_view, std::string_view);

private:
  void on_error(boost::system::error_code const&);
  void do_connect(boost::asio::ip::tcp::resolver::results_type const&);
  void do_read();
  void do_write(size_t);

private:
  boost::asio::ip::tcp::resolver r_;
  boost::asio::ip::tcp::socket s_;
  boost::asio::streambuf buf_;
};

}  // namespace mcf::example

#endif  // _MCF_EXAMPLE_INTERPRETER_HPP_
