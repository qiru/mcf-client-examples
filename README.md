# MCF-Client-Examples

Repository intended to show how to write a MCF client in C++.

## Dependencies

[Boost](https://www.boost.org/) 1.76.0 or later is mandatory for building the examples.


## Building

[CMake](https://www.cmake.org/) is used to build all examples. Here's an example of building procedure on Windows/MSVC:

```
C:\>cmake -G "Visual Studio 17 2022" -DBOOST_ROOT=path\to\boost -B %TEMP%\MCF-EXAMPLE .
C:\>cmake --build %TEMP%\MCF-EXAMPLE --config Debug
```
