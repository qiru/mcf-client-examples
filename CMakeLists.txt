cmake_minimum_required(VERSION 3.12.0 FATAL_ERROR)
project(MCF-Examples CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

find_package(Boost 1.76.0 REQUIRED COMPONENTS system REQUIRED)

add_subdirectory(batch)
add_subdirectory(interpreter)
